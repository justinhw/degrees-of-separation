import ast
import os

from flasgger import Swagger
from flask import Flask

from config import Config
from degrees_of_separation.services.cold_start import cold_start_db

swagger = Swagger()


def create_app(config=Config):
    app = Flask(__name__)
    app.config.from_object(config)
    swagger.init_app(app)

    with app.app_context():
        from degrees_of_separation import routes
        from degrees_of_separation.services import bp as services_bp
        app.register_blueprint(services_bp)

        if ast.literal_eval(os.getenv('COLD_START', '1')):
            cold_start_db()

        return app
