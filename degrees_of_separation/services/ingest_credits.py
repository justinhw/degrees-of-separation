from typing import List, Dict

from degrees_of_separation.services.neo4j import Neo4J


def ingest_credits(credits: Dict[str, List]):
    """
    Idempotent func that upserts new movies and actors that acted in those movies into the database

    Strips surrounding white spaces for movie names and actor full names and converts to lowercase

    :param credits: dictionary of movie names to a list of actors full names that acted in the movie
    :return: None
    """

    neo4j = Neo4J()
    movie_names = list({movie_name.strip().lower() for movie_name in list(credits.keys())})
    movie_names_to_ids = neo4j.batch_upsert_movies_using_only_name(movie_names)

    actor_names = set()
    for names in list(credits.values()):
        actor_names.update(set([name.strip().lower() for name in names]))
    actor_names_to_ids = neo4j.batch_upsert_actors_using_only_names(list(actor_names))

    relationships = set()
    for movie_name, actors in credits.items():
        for actor_name in actors:
            relationships.add((movie_names_to_ids[movie_name.strip().lower()], actor_names_to_ids[actor_name.strip().lower()]))

    neo4j.batch_upsert_acted_in_relationships(list(relationships))
