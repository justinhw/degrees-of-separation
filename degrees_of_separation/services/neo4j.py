import os
import uuid
from typing import List, Tuple, Dict

from neo4j import GraphDatabase

from degrees_of_separation.services.utils import singleton

DEFAULT_NEO4J_HOST_URI = "neo4j://localhost:7687"


@singleton
class Neo4J:
    def __init__(self):
        # for PoC purposes, neo4j is set to no auth required
        self.driver = GraphDatabase.driver(
            uri=os.getenv("NEO4J_HOST_URI", DEFAULT_NEO4J_HOST_URI)
        )

    def close(self):
        self.driver.close()

    def upsert_constraints_and_indices(self):
        """
        Idempotent method that upserts unique constraints on the person and movie entities

        :return: None
        """

        with self.driver.session() as session:
            def write_unique_person_name_constraint(tx):
                tx.run(
                    "CREATE CONSTRAINT UNIQUE_PERSON_ID IF NOT EXISTS ON (p:Person) ASSERT p.id IS UNIQUE; "
                )

            def write_unique_movie_id_constraint(tx):
                tx.run(
                    "CREATE CONSTRAINT UNIQUE_MOVIE_ID IF NOT EXISTS ON (m:Movie) ASSERT m.id IS UNIQUE; "
                )

            def write_person_name_index(tx):
                tx.run(
                        "CREATE INDEX person_id_idx IF NOT EXISTS FOR (p:Person) ON (p.id); "
                )

            def write_movie_name_index(tx):
                tx.run(
                    "CREATE INDEX movie_id_idx IF NOT EXISTS FOR (m:Movie) ON (m.id); "
                )

            session.write_transaction(write_unique_person_name_constraint)
            session.write_transaction(write_unique_movie_id_constraint)
            session.write_transaction(write_person_name_index)
            session.write_transaction(write_movie_name_index)

    def batch_upsert_actors(self, actors: List[Tuple]):
        """
        Idempotent method that upserts actors

        Note that actors only have a name attribute

        :param actors: list of actor tuples (id, name) - case sensitive
        :return: None
        """

        actors_transformed = [{'id': str(a[0]), 'name': a[1]} for a in actors]

        with self.driver.session() as session:
            def write(tx, data):
                tx.run(
                    "WITH $data as data "
                    "UNWIND data as d "
                    "MERGE (p:Person {name: d.name, id: d.id}) ",
                    data=data
                )

            session.write_transaction(write, actors_transformed)

    def batch_upsert_movies(self, movies: List[Tuple]):
        """
        Idempotent method that batch upserts movies (updates if exists otherwise creates)

        :param movies: list of movie tuples (movie id, movie name)- case sensitive
        :return: none
        """

        movies_transformed = [{'id': str(m[0]), 'name': m[1]} for m in movies]

        with self.driver.session() as session:
            def write(tx, data):
                 tx.run(
                    "WITH $data as data "
                    "UNWIND data as d "
                    "MERGE (m:Movie {name: d.name, id: d.id}) ",
                     data=data
                 )

            session.write_transaction(write, movies_transformed)

    def batch_upsert_movies_using_only_name(self, movie_names: List[str]) -> Dict[str, str]:
        """
        Idempotent method that batch upserts movies based on name
        If a movie with the same name already exists then nothing is created. If no such movie exists,
        then a new emovie id is created alongside the movie name.

        TODO: consumer API should probably accept movie id on top of name

        :param movie_names: list of movie names case sensitive
        :return: map of upserted movies { movie name: movie id }
        """

        movies_transformed = [{'id': str(uuid.uuid4()), 'name': movie_name} for movie_name in movie_names]

        with self.driver.session() as session:
            def write(tx, data):
                results = tx.run(
                    "WITH $data as data "
                    "UNWIND data as d "
                    "MERGE (m:Movie {name: d.name}) "
                    "ON CREATE SET m.id = d.id "
                    "RETURN m.id, m.name",
                     data=data
                )
                return {r[1]: r[0] for r in results}

            return session.write_transaction(write, movies_transformed)

    def batch_upsert_actors_using_only_names(self, actors_names: List[str]) -> Dict[str, str]:
        """
        Idempotent method that batch upserts actors based on name

        TODO: consumer API should probably accept actor id on top of name

        :param actors_names: list of actors names case sensitive
        :return: map of upserted actors { actor name: actor id }
        """

        actors_transformed = [{'id': str(uuid.uuid4()), 'name': actor_name} for actor_name in actors_names]

        with self.driver.session() as session:
            def write(tx, data):
                results = tx.run(
                    "WITH $data as data "
                    "UNWIND data as d "
                    "MERGE (a:Person {name: d.name}) "
                    "ON CREATE SET a.id = d.id "
                    "RETURN a.id, a.name",
                     data=data
                )
                return {r[1]: r[0] for r in results}

            return session.write_transaction(write, actors_transformed)

    def batch_upsert_acted_in_relationships(self, acted_in_relationships: List[Tuple]):
        """
        Idempotent method that batch upserts relationships (movies that an actor acted in) matching on movie and actor ids

        :param acted_in_relationships: list of tuples (movie id, actor id)
        :return: None
        """

        acted_in_relationships_transformed = [{'movie_id': rel[0], 'actor_id': rel[1]} for rel in acted_in_relationships]
        with self.driver.session() as session:
            def write(tx, relationships):
                tx.run(
                    "WITH $relationship as relationship "
                    "UNWIND relationship as rel "
                    "MATCH (p:Person), (m:Movie) WHERE p.id = rel.actor_id and m.id = rel.movie_id MERGE (p)-[:ACTED_IN]->(m)",
                    relationship=relationships
                )

            session.write_transaction(write, acted_in_relationships_transformed)

    def get_degrees_of_separation(self, source_actor: str, dest_actor: str) -> int:
        """
        Returns the degrees of separation between two actors - variant of https://en.wikipedia.org/wiki/Six_Degrees_of_Kevin_Bacon

        TODO: this should probably be matching based on actor ids instead of names

        :param source_actor: source actor full name - case sensitive
        :param dest_actor: destination actor full name - case sensitive
        :return: degree of separation. 0 means the two actors have no shared paths. 1 means they were in the same movie together. And so on.
        """

        with self.driver.session() as session:
            result = session.run(
                "MATCH p=shortestPath((source:Person {name:$source})-[*]-(dest:Person {name:$dest})) "
                "WITH nodes(p) as ns "
                "RETURN [n IN ns WHERE n:Person] as result",
                source=source_actor,
                dest=dest_actor
            ).single()

            if not result:
                return 0

            result = result[0]

            # result would include the source and destination nodes
            # if two actors are in the same movie, it should be a degree of 1
            degrees_of_separation = len(result) - 1

            return degrees_of_separation if degrees_of_separation >= 0 else 0
