import ast
import os
from typing import Set, Tuple, Dict

import pandas

from degrees_of_separation.services.neo4j import Neo4J


BATCH_SIZE = ast.literal_eval(os.getenv('BATCH_SIZE', '30000'))
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
MOVIES_METADATA_PATH = BASE_DIR + '/../resources/movies_metadata.csv'
CREDITS_PATH = BASE_DIR + '/../resources/credits.csv'


def cold_start_db():
    """
    Idempotent func that initializes the database with basic movies and actors data needed to drive the degrees of separation problem
    It loads data mainly from credits.csv and movies_metadata.csv (source https://www.kaggle.com/rounakbanik/the-movies-dataset?select=movies_metadata.csv)

    Note: the cold start could take about a minute to complete depending on how much memory is allocated to the heap (recommendation is 2 gb min.)

    :return: None
    """

    print("Cold start - loading db with data ...")

    neo4j = Neo4J()
    neo4j.upsert_constraints_and_indices()

    jobs = _get_jobs(neo4j)

    for job in jobs:
        print(f"Loading {len(job['data'])} {job['name']}")

        batches = _to_batches(job['data'], BATCH_SIZE)

        for i, batch in enumerate(batches):
            print(f"Processing {i + 1}/{len(batches)} batches")
            job_func = job['func']
            job_func(batch)
            print(f"Processed {len(batch)} {job['name']}")

    print("Db loaded with data!")


def _to_batches(raw_list, batch_size):
    return [raw_list[i:i + batch_size] for i in range(0, len(raw_list), batch_size)]


def _get_movies_metadata():
    df = pandas.read_csv(MOVIES_METADATA_PATH)
    metadata = {}

    for movie_id, movie_title in zip(df.id, df.original_title):
        metadata[str(movie_id)] = movie_title.strip().lower()

    return metadata


def _get_jobs(neo4j):
    movies_metadata = _get_movies_metadata()
    df = pandas.read_csv(CREDITS_PATH)
    # scope of this project is just the cast (actors)
    df['cast'] = df.cast.apply(ast.literal_eval)

    movies: Set[Tuple] = set()  # (movie id, movie name)
    actor_ids_to_names: Dict = {}
    acted_in_relationships: Set[Tuple] = set()  # (movie id, actor name)

    for movie_cast, movie_id in zip(df.cast, df.id):
        movie_id = str(movie_id)
        movies.add((movie_id, movies_metadata[movie_id].strip().lower()))

        for actor_cast_detail in movie_cast:
            actor_id = str(actor_cast_detail['id'])

            if actor_id not in actor_ids_to_names:
                actor_ids_to_names[actor_id] = actor_cast_detail['name'].strip().lower()

            acted_in_relationships.add((movie_id, actor_id))

    actors = [(k, v) for k, v in actor_ids_to_names.items()]

    return [
        {
            'name': 'movies',
            'func': neo4j.batch_upsert_movies,
            'data': list(movies)
        },
        {
            'name': 'actors',
            'func': neo4j.batch_upsert_actors,
            'data': list(actors)
        },
        {
            'name': 'acted in relationships',
            'func': neo4j.batch_upsert_acted_in_relationships,
            'data': list(acted_in_relationships)
        }
    ]
