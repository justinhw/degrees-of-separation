from degrees_of_separation.services.neo4j import Neo4J


class InvalidParamsError(Exception):
    pass


def get_degrees_of_separation(source_actor: str, destination_actor: str):
    """
    Gets the degrees of separation between two actors

    :param source_actor: actor full name - case insensitive and surrounding white spaces will be stripped
    :param destination_actor: actor full name - case insensitive and surrounding white spaces will be stripped
    :return: degree of separation. 0 means the two actors have no shared paths. 1 means they were in the same movie together. And so on.
    """

    source_actor = source_actor.strip().lower()
    destination_actor = destination_actor.strip().lower()

    if not source_actor or not destination_actor:
        raise InvalidParamsError("Source and destination actors must both be present")

    return Neo4J().get_degrees_of_separation(source_actor=source_actor, dest_actor=destination_actor)
