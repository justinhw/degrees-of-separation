import pytest
from mock import MagicMock, patch

from degrees_of_separation.services.get_degrees_of_separation import get_degrees_of_separation, InvalidParamsError


class TestGetDegreesOfSeparation:

    @pytest.fixture()
    def mock_neo4j(self):
        mock = MagicMock()

        with patch("degrees_of_separation.services.get_degrees_of_separation.Neo4J", return_value=mock):
            yield mock

    def testGetDegreesOfSeparation_givenWhiteSurroundingSpaces_expectStrippedAndLowered(self, mock_neo4j):
        get_degrees_of_separation(source_actor=" Tom hAnks", destination_actor=" CLINT EASTWOOD  ")

        mock_neo4j.get_degrees_of_separation.assert_called_with(source_actor="tom hanks", dest_actor="clint eastwood")

    def testGetDegreesOfSeparation_givenMissingActor_expectException(self):
        with pytest.raises(InvalidParamsError):
            get_degrees_of_separation(source_actor="", destination_actor="tom hanks")

        with pytest.raises(InvalidParamsError):
            get_degrees_of_separation(source_actor="a", destination_actor="  ")