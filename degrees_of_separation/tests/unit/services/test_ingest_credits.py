import pytest
from mock import MagicMock, patch

from degrees_of_separation.services.ingest_credits import ingest_credits


class TestIngestCredits:

    @pytest.fixture()
    def mock_neo4j(self):
        mock = MagicMock()

        with patch("degrees_of_separation.services.ingest_credits.Neo4J", return_value=mock):
            yield mock

    def testIngestCredits_givenSpacesAndCappedNames_expectStrippedAndLowered(self, mock_neo4j):
        credits = {
            'justin wong movie ': [' Justin WONG  ', 'Justino wonG'],
            ' JUSTINO WONG MOVIE': ['justin wong', 'JUSTINO WONG ']
        }
        ingest_credits(credits)

        # TODO: fix this hacky way of validating the params are correct - the alternative is to sort it but this seems ok for now
        batch_upsert_movies_using_only_name_params = mock_neo4j.mock_calls[0][1][0]
        batch_upsert_actors_using_only_name_params = mock_neo4j.mock_calls[1][1][0]

        assert set(batch_upsert_movies_using_only_name_params) == {'justin wong movie', 'justino wong movie'}
        assert set(batch_upsert_actors_using_only_name_params) == {'justin wong', 'justino wong'}
        mock_neo4j.batch_upsert_acted_in_relationships.assert_called_once()
