from typing import NamedTuple

from flask import jsonify, request

from degrees_of_separation.services import bp
from degrees_of_separation.services.get_degrees_of_separation import get_degrees_of_separation, InvalidParamsError
from degrees_of_separation.services.ingest_credits import ingest_credits

DEGREES_OF_SEPARATION_ENDPOINT = '/separation'
CREDITS_ENDPOINT = "/credits"


class SingleMessageResponse(NamedTuple):
    message: str


class Response(NamedTuple):
    message: str
    degrees: int


@bp.route(DEGREES_OF_SEPARATION_ENDPOINT, methods=['GET'])
def handle_degrees_of_separation_request():
    """
    Get degrees of separation between two actors


    Degree of 0 means there is no path between two actors.
    Degree of 1 means the actors acted in the same movie.
    Degree of 2 means the actors acted in a movie that a mutual actor acted in.
    And so on.

    ---
    tags:
        - Movies
    parameters:
        - name: source_actor
          in: query
          description: case-insensitive - surrounded white spaces will be stripped
          required: true
          type: string
        - name: destination_actor
          in: query
          description: case-insensitive - surrounded white spaces will be stripped
          required: true
          type: string
    responses:
        500:
            description: Internal server error
        400:
            description: Bad request
            schema:
                id: single_message_resp
                type: object
                properties:
                    message:
                        type: string
        404:
            description: One or more of the actors do not exist
            schema:
                $ref: '#/definitions/single_message_resp'
        200:
            description: Successfully got degrees of separation
            schema:
                id: degrees_of_separation_resp
                type: object
                properties:
                    message:
                        type: string
                    data:
                        type: integer
    """

    try:
        result = get_degrees_of_separation(
            source_actor=request.args.get('source_actor'),
            destination_actor=request.args.get('destination_actor')
        )
        return jsonify(Response(message="Successfully got degrees of separation", degrees=result)._asdict()), 200
    except InvalidParamsError as e:
        return jsonify(SingleMessageResponse(message=e.args[0])._asdict()), 400


@bp.route(CREDITS_ENDPOINT, methods=['POST'])
def handle_new_movies_request():
    """
    Upserts new credits (movies and actors that acted in that movie) to the database

    Note: since actor names and movie names are used instead of ids the upsert matches based on lowercase movie name and actor full names

    Sample request body:

    `{"Justin Wong Movie": ["Justin Wong", "Justin2 Wong"],"Justin Wong2 Movie": ["Justin Wong", "Justin2 Wong"]}`
    ---
    tags:
        - Movies
    parameters:
        - name: credits
          in: body
          description: Map of movie name (case insensitive) to list of actors full names that acted in the movie (case insensitive). Surrounding white spaces for movie name and actor names will be stripped.
          required: true
          schema:
            type: object
            properties:
                movie_name:
                    type: array
                    items:
                        type: string
    responses:
        500:
            description: Internal server error
        400:
            description: Bad request
            schema:
                $ref: '#/definitions/single_message_resp'
        200:
            description: Successfully upserted movies and actors
            schema:
                $ref: '#/definitions/single_message_resp'
    """

    try:
        ingest_credits(request.json)
        return jsonify(SingleMessageResponse(message="Successfully ingested new credits")._asdict()), 200
    except Exception as e:
        print(f"Unexpected error {e.args[0]}")
        return jsonify(SingleMessageResponse(message="Unexpected error. Please try again later")._asdict()), 500
