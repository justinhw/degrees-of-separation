from degrees_of_separation import create_app

app = create_app()


if __name__ == "__main__":
	print("Db loaded with data! Checkout Neo4J to view the data (default: http://localhost:7474/browser/) ")
	print("Swagger API spec: http://localhost:5000/apidocs")
	app.run("localhost", port=5000)
