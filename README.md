
[![pipeline status](https://gitlab.com/justinhw/degrees-of-separation/badges/master/pipeline.svg)](https://gitlab.com/justinhw/degrees-of-separation/-/commits/master)
[![coverage report](https://gitlab.com/justinhw/degrees-of-separation/badges/master/coverage.svg)](https://gitlab.com/justinhw/degrees-of-separation/-/commits/master)

# Degrees of Separation

Built under the intention that:
- Cold starts would rarely happen 
- Persistence is desired to minimize cold starts and to ensure that previously ingested data can be used even when the app is restarted
- API matching based on names is ok for now but probably not for long-term
- Movies data is valid (i.e. Clint Eastwood really did act in the same movie as Morgan Freeman)
- Lean - only storing data that is absolutely necessary to solve this problem
- Purely PoC with no foreseeable traffic - no need for http server + reverse proxy or anything fancy as of yet

Built with:
- Neo4J (ACID): https://neo4j.com/
- Python 3
- Flask
- Swagger

# Setup

Requirements:
- Docker
- Python 3

1. Run Neo4J (graph db) in a Docker container. Note: I'd recommend at least 1GB for the heap and 2GB for the pagecache otherwise it'll take a long time to cold start the service with the movies data.

    ```
    docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --env=NEO4J_AUTH=none \
    --env=NEO4J_dbms_memory_heap_max__size=1G \
    --env=NEO4J_dbms_memory_pagecache_size=2G \
    neo4j
    ```

2. Navigate to http://localhost:7474 to access data via Neo4J console (no auth)
3. Setup Python virtual env and install dependencies
   ```
   # Create python3 virtualenv
   $ python3 -m venv venv
   
   # Install reqs
   $ pip install -r requirements --no-cache-dir
   ```
4. Setup environment variables

| **Env. Variable** | **Value** |
| --- | --- |
| COLD_START | 0 or 1 (default) - 0 (no cold start, your Neo4J instance already has all the data) or 1 (cold start, your Neo4J doesn't have data or data is incomplete) 
| NEO4J_HOST_URI | Defaults to neo4j://localhost:7687 |
| BATCH_SIZE | Defaults to 30000 - only affects cold start |

5. Run the app (runs on port 5000)

   ```
   $ python run.py
   ```
6. Navigate to http://localhost:5000/apidocs to see the Swagger API spec and to interact with the APIs

# Future Improvements

- APIs should ingest new credits and do a degree of separation based on actor and movie ids instead of names since multiple actors/movies could share the same name
- Improve error handling
- Setup a CD pipeline to deploy app + neo4j to the cloud (e.g. Heroku, AWS, etc.) 
- Improve the cypher query that powers degrees of separation. This works right now because we only have 1 kind of relationship: ACTED_IN. If we added new relationships in the future we'd want to filter the path based on only ACTED_IN relationships.
- Add auth. layer so that only allowed consumers are able to access the APIs (e.g. could use something like AWS Cognito)
- Scale and re-architect as necessary depending on usage expectations (e.g. load balancer? cache? api gateway?)
- Since Neo4J is NoSql, we could easily add more attributes to the person, movie or acted_in models as we learn more about what those requirements are. Choosing SQL at this moment would've complicated matters and slowed down development so early on in the process since we don't have the requirements fully fleshed out yet.
- Use a real production http server + reverse proxy (e.g. nginx + gunicorn)
- Proper logging
- Analytics
- Alerts & monitoring
- Integration tests
- Setup auth, roles and permissions around Neo4j
